import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;

public class Main {

    public static void main(String[] args) {
	// write your code here
        try {
            RandomAccessFile randomAccessFile = new RandomAccessFile("src/files/text.txt", "rw");
//            randomAccessFile.writeUTF("Hello");

            randomAccessFile.seek(3);
            byte[] b = new byte[5];
            randomAccessFile.read(b);

            System.out.println(new String(b));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


}
