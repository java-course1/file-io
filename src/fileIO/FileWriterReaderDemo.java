package fileIO;

import models.Student;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class FileWriterReaderDemo {

    private String filePath = "src/files/text.txt";

    public static void main(String[] args) {
        FileWriterReaderDemo fileWriterReaderDemo =
                new FileWriterReaderDemo();

        fileWriterReaderDemo.writeData();
//        fileWriterReaderDemo.readData();
    }

    void writeData () {
        // 1- create object from FileWriter
        try {
             FileWriter fileWriter = new FileWriter(filePath, true);

            //2- write data into file
            Student student = new Student(2, "kaka 2", "female", "2-2-2000");

            fileWriter.write(student + "\n");
            fileWriter.flush();
            fileWriter.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    void readData () {
        // 1- create object from FileReader
        try {
            FileReader fileReader = new FileReader(filePath);
            int i;
            String studentFields = "";
            String[] fields;
            String[] student;

            //2- read data from file
            while ((i = fileReader.read()) != -1) {
                studentFields += (char)i;
            }

            fields = studentFields.split("\n");
            for (int ii=0; ii<fields.length; ii++) {
                System.out.println(fields[ii]);

                student = fields[ii].split(",");
                System.out.println(student[1]);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
