package fileIO;

import models.Student;

import java.io.*;
import java.util.ArrayList;

public class FileObjectDemo {

    private String filePath = "src/files/text.txt";

    public static void main(String[] args) {
        FileObjectDemo objectDemo = new FileObjectDemo();
//        objectDemo.writeData();
        objectDemo.readData();
    }

    void writeData () {
        try {
            FileOutputStream fileOutputStream = new FileOutputStream(filePath, true);
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream) {
                @Override
                protected void writeStreamHeader() throws IOException {
//                    super.writeStreamHeader();
                }
            };

            objectOutputStream.writeObject(new Student(1, "hello", "male", "2000"));
            objectOutputStream.flush();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    void readData () {
        try {
            ObjectInputStream objectInputStream = new ObjectInputStream(new FileInputStream(filePath)) {
                @Override
                protected void readStreamHeader() throws IOException {
//                    super.readStreamHeader();
                }
            };

//            Student student = (Student) objectInputStream.readObject();
//            System.out.println(student);
            Student student1;
            while ((student1 = (Student) objectInputStream.readObject()) != null) {
                System.out.println(student1);
            }
        } catch (EOFException e) {
//            System.out.println(e.getMessage());
        }  catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

}
