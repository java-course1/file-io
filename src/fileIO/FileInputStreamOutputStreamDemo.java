package fileIO;

import java.io.*;

public class FileInputStreamOutputStreamDemo {

    private String filePath = "src/files/text.txt";
    public static void main(String[] args) {
        FileInputStreamOutputStreamDemo fileInputStreamOutputStreamDemo =
                new FileInputStreamOutputStreamDemo();
        // fileInputStreamOutputStreamDemo.createFile();
        // fileInputStreamOutputStreamDemo.writeData();
        fileInputStreamOutputStreamDemo.readData();
    }

    // create file
    void createFile () {

        // 1- create object of file
        File file = new File(filePath);

        // 2- check if file is existed or not
        if (file.exists()) {
            System.out.println("File is already existed");
        } else {
            System.out.println("File is not yet existed");
            try {
                // 3- if no => create new file
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    void writeData () {
        try {

            // 1- create object from FileOutputStream
            FileOutputStream fileOutputStream = new FileOutputStream(filePath);

            // 2- convert string to array of bytes then write into file
//            fileOutputStream.write("Java Weekend".getBytes());
             String st = "Java Weekend";
             byte[] b = st.getBytes();
             fileOutputStream.write(b);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    void readData () {


            // 1- create object from FileInputStream
        FileInputStream fileInputStream = null;
        try {
            fileInputStream = new FileInputStream("src/files/text.txt");
            int i;

            // 2- read data from file
            while ((i = fileInputStream.read()) != -1) {
                System.out.println((char)i);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }


    }
}
